## Algorithm class test code

Tutorial links:

* [Binary search tree](https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/)
* [Breadth first search](https://www.geeksforgeeks.org/breadth-first-search-or-bfs-for-a-graph/)
* [Depth first search](https://www.geeksforgeeks.org/depth-first-search-or-dfs-for-a-graph/)
* [Prim's Algorithm](https://www.geeksforgeeks.org/prims-minimum-spanning-tree-mst-greedy-algo-5/)
* [Krushkal's Algorithm](https://www.geeksforgeeks.org/kruskals-algorithm-simple-implementation-for-adjacency-matrix/)
* [Dijkstra's Algorithm](https://www.geeksforgeeks.org/dijkstras-shortest-path-algorithm-greedy-algo-7/)

